//
//  FeedsViewController.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import UIKit

class FeedsViewController: UIViewController {

   // let tableView = UITableView()
    private let refreshControl = UIRefreshControl()
    let tableView = UITableView()
    
    fileprivate var service : FeedsService! = FeedsService()
    let dataSource = FeedsDataSource()
    lazy var viewModel : FeedsViewModel = {
        let viewModel = FeedsViewModel(service: service, dataSource: dataSource)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  view.addSubview(TableView.shared.addConstraint(tableView, self))
        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self.dataSource
               self.dataSource.data.addAndNotify(observer: self) { [weak self] in
                   self?.tableView.reloadData()
               }
               self.setupUI()
               self.setupUIRefreshControl()
               self.serviceCall()
    }
    
    func setupUIRefreshControl(){
        refreshControl.addTarget(self, action: #selector(serviceCall), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl)
        
    }
    @objc func serviceCall() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.viewModel.fetchServiceCall { result in
                switch result {
                case .success :
                    self.title = self.viewModel.title
                    break
                case .failure :
                    break
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        refreshControl.endRefreshing()
    }
}

extension FeedsViewController {
    func setupUI() {
        tableViewSetup()
        self.tableView.backgroundColor = .white
        self.view.backgroundColor = .white
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: self, action: #selector(serviceCall))
    }
    
    func tableViewSetup() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo:view.safeAreaLayoutGuide.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.register(LandscapeTableViewCell.self, forCellReuseIdentifier: ConstantValue.tableViewCellReuseable )
        
    }
}

//// MARK: - TableViewDelegate Setup

extension FeedsViewController : UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

