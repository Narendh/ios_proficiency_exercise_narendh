//
//  FeedsDataSource.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
    fileprivate let landscapeReuseIdentifier = ConstantValue.tableViewCellReuseable
}

class FeedsDataSource : GenericDataSource<ListModel>, UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: landscapeReuseIdentifier, for: indexPath) as! LandscapeTableViewCell
        
        let feedsValue = self.data.value[indexPath.row]
        cell.feedsValue = feedsValue
        
        if let reachability = Reachability(), !reachability.isReachable {
            cell.thumbnailImage.image =  CoreDataHandler.shared.fetchImageData(title: feedsValue.title)
        }
        else{
        if feedsValue.imageRef != ""  && feedsValue.imageRef != "N/A"{
            ImageHelper().updateImageForTableViewCell(cell, inTableView: tableView, imageURL:feedsValue.imageRef, atIndexPath: indexPath){ (success, image) -> Void in
                if success && image != nil {
                    // cell.thumbnailImage.isHidden = false
                    cell.thumbnailImage.image = image
                }else{
                    // cell.thumbnailImage.isHidden = true
                    cell.thumbnailImage.image = UIImage(named: ConstantValue.placehoderImage)!
                }
            }
        }else{
            // cell.thumbnailImage.isHidden = true
            // cell.imageWidthConstraint.constant = 0
            cell.thumbnailImage.image = UIImage(named: ConstantValue.placehoderImage)!
            
        }
    }
        
        
        
        return cell
    }
}
