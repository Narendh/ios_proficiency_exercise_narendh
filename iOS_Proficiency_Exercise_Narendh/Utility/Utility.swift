//
//  Utility.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import Foundation

protocol Utility{
    func filterNil(_ value : AnyObject?) -> AnyObject?
}

class Util {
    
}

extension Util: Utility {
    func filterNil(_ value : AnyObject?) -> AnyObject? {
        if value is NSNull || value == nil {
            return "N/A" as AnyObject
        } else {
            return value
        }
    }
}
