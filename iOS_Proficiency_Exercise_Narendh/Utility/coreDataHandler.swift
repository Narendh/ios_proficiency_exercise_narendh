//
//  coreDataHandler.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import UIKit
import CoreData


class CoreDataHandler: NSObject {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate //Singlton instance
    var context:NSManagedObjectContext!
    
    static let shared = CoreDataHandler()
    override init() {
        context = appDelegate.persistentContainer.viewContext
    }
    
    
    func saveData(list:[ListModel])
    {
      //  DispatchQueue.global(qos: .background).async {
        
        if entityIsEmpty() {
            var imageManager: ImageManager { return ImageManager() }
            for i in list {
                imageManager.downloadImageFromURL(i.imageRef) { (success, image) -> Void in
                    let entity = NSEntityDescription.entity(forEntityName: "List", in: self.context)!
                    let UserDBObj = NSManagedObject(entity: entity, insertInto: self.context)
                    if success {
                        //if self.duplicate(title: i.title) {
                            UserDBObj.setValue(i.title, forKey: "titleName")
                            UserDBObj.setValue(i.description, forKey: "disp")
                            if let dataImg = image?.pngData() {
                                UserDBObj.setValue(dataImg, forKey: "imageRef")
                                print("Storing Data..")
                                do {
                                    try self.context.save()
                                } catch {
                                    print("Storing data Failed")
                                }
                            }
                        }
                    else {
                        UserDBObj.setValue(i.title, forKey: "titleName")
                        UserDBObj.setValue(i.description, forKey: "disp")
                    }
                   // }
                }
            }
        }
    }
    
    func fetchImageData(title:String) -> UIImage
    {
        print("Fetching Data..")
        var image = UIImage(named: ConstantValue.placehoderImage)!
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "List")
        request.predicate = NSPredicate(format:"titleName = %@", title)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let dataImg:Data = data.value(forKey: "imageRef") as? Data {
                    image = UIImage(data: dataImg)!
                }
            }
        } catch {
            print("Fetching data Failed")
            
        }
        return image
    }
    
    
    
    func deleteData(title:String,completionHandler: (Bool) -> Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "List")
        request.predicate = NSPredicate(format:"titleName = %@", title)
        if let results = try! context.fetch(request) as? [NSManagedObject] {
            print(results.count)
            for result in results {
                context.delete(result)
            }
        } else {
            print("Failed")
            completionHandler(false)
        }
        do {
            try context.save()
            completionHandler(true)
        } catch {
            print("Storing data Failed")
            completionHandler(false)
        }
        
    }
    
    func duplicate(title:String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "List")
        request.predicate = NSPredicate(format:"titleName = %@", title)
        if let results = try! context.fetch(request) as? [NSManagedObject] {
            if entityIsEmpty() {
                return true
            }
            else if results.count > 0 {
                return true
            }
            else {
                return false
            }
            
        } else {
            print("Failed")
            return false
        }
    }
    
    
    func entityIsEmpty() -> Bool
    {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "List")
            let count  = try context.count(for: request)
            return count == 0
        } catch {
            return true
        }
    }
    
    
}
