//
//  FeedsService.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import Foundation

protocol FeedsServiceProtocol : class
{
    func fetchConverter(_ completion: @escaping ((Result<FeedsModel, ErrorResult>) -> Void))
}

final class FeedsService : RequestHandler, FeedsServiceProtocol
{
    let endpoint = ConstantValue.BaseURL
    var task : URLSessionTask?
    
    func fetchConverter(_ completion: @escaping ((Result<FeedsModel, ErrorResult>) -> Void))
    {
        self.cancelFetchCurrencies()
        task = RequestService().loadData(urlString: endpoint, completion: self.networkResult(completion: completion))
    }
    
    func cancelFetchCurrencies()
    {
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}


