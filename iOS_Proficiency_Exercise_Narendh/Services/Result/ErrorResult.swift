//
//  ErrorResult.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}
