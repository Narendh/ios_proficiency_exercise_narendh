//
//  LandscapeTableViewCell.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 19/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import UIKit

class LandscapeTableViewCell: UITableViewCell {
    
    let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        return view
    }()
    
    let thumbnailImage:UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill // image will never be strecthed vertially or horizontally
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.layer.cornerRadius = 30
        img.clipsToBounds = true
        img.tag = 1
        img.image = UIImage(named: ConstantValue.placehoderImage)!
        return img
    }()
    
    let titleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor =  .black
        label.backgroundColor = .clear
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var feedsValue : ListModel? {
        didSet {
            guard let feeds = feedsValue else {
                return
            }
            titleLabel.text = feeds.title
            descriptionLabel.text = feeds.description
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(thumbnailImage)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(descriptionLabel)
        
        thumbnailImage.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
        thumbnailImage.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        thumbnailImage.widthAnchor.constraint(equalToConstant:60).isActive = true
        thumbnailImage.heightAnchor.constraint(equalToConstant:60).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo:self.self.contentView.topAnchor,constant: 10).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant:20).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo:self.thumbnailImage.trailingAnchor,constant: 8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor,constant: 10).isActive = true
        
        descriptionLabel.topAnchor.constraint(equalTo:self.titleLabel.bottomAnchor,constant: 10).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo:self.titleLabel.leadingAnchor).isActive = true
        
        descriptionLabel.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor,constant: -10).isActive = true
        descriptionLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 40).isActive = true
        descriptionLabel.sizeToFit()
        descriptionLabel.numberOfLines = 0
        
        self.contentView.bottomAnchor.constraint(equalTo: descriptionLabel.bottomAnchor,constant: 10).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.titleLabel.topAnchor,constant: -10).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.thumbnailImage.contentMode =   UIView.ContentMode.scaleAspectFit
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
