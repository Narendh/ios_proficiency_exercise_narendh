//
//  Constant.swift
//  iOS_Proficiency_Exercise_Narendh
//
//  Created by APPLE on 20/07/20.
//  Copyright © 2020 Narendh. All rights reserved.
//

import Foundation

struct ConstantValue {
    
    static let BaseURL = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    static let tableViewCellReuseable = "LandscapeTableViewCell"
    static let placehoderImage = "placeholder"
}

